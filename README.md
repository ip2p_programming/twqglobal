# twqglobal

When setting up:

1) Check the database. If empty, run: python3 manage.py migrate
2) Create a superuser: python3 manage.py createsuperuser
3) Load the token meta fixtures: python3 loaddata tokenmeta_fixtures.json
4) Put the etherscan update on cron.
5) Put the account update on cron.