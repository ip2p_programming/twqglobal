from django import forms
from django.utils.translation import ugettext_lazy as _

class LoginForm(forms.Form):
    email = forms.EmailField(
        label='E-mail / 邮箱',
        widget=forms.TextInput(),
        max_length=255,
    )
    password = forms.CharField(
        widget=forms.PasswordInput(),
        label='Password / 密码', 
        max_length=255,
    )

class BuyForm(forms.Form):
    wallet = forms.CharField(
        label="ImToken Wallet Address / 输入自己IMTOKEN钱包地址",
        widget=forms.TextInput(),
        max_length=255,
    )
    ether_amount = forms.FloatField(label="ETH")
    """
    token_amount = forms.DecimalField(
        label="TWQ Amount / 输入购买TWQ数量",
        decimal_places=2,
        max_digits=10,
    )
    """
class TransferForm(forms.Form):
    recipient = forms.CharField(
        label="E-mail/Member ID / 邮箱/会员号",
        widget=forms.TextInput(),
        max_length=255,
    )
    token_amount = forms.DecimalField(
        label="TWQ Amount / 输入购买TWQ数量",
        decimal_places=2,
        max_digits=10,
    )
    wallet = forms.CharField(
        label="ImToken Wallet Address / 输入自己IMTOKEN钱包地址",
        widget=forms.TextInput(),
        max_length=255,
    )    
