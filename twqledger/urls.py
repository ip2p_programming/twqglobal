from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('portfolio/<member>', views.investor, name='portfolio'),
    path('team/<member>', views.teamlead, name='team'),
    path('master/<member>', views.master, name='master'),
    path('logout', views.logout, name='logout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
