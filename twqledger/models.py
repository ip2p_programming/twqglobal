from django.db import models
from django.forms import ModelForm, PasswordInput, TextInput
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

class InvestorProfile(models.Model):
    email = models.EmailField(primary_key=True)
    member_id = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=20)
    password = models.CharField(max_length=255)
    password_confirm = models.CharField(max_length=255)
    referral = models.CharField(blank=True, max_length=255)
    membership = models.CharField(max_length=15)
    logged_on = models.BooleanField(default=False)

class RegistrationForm(ModelForm):    
    class Meta:
        model = InvestorProfile
        fields = ['email', 'password', 'password_confirm', 'phone_number', 
            'referral']
        widgets = {
            'password': PasswordInput(),
            'password_confirm': PasswordInput(),
            'email': TextInput(),
            'phone_number': TextInput(),
            'referral': TextInput(),
        }
        labels = {
            'email': 'E-mail / 邮箱',
            'password': 'Password / 密码',
            'password_confirm': 'Re-type Password / 确认密码',
            'phone_number': 'Mobile Number / 手机号码',
            'referral': 'Referral ID / 邀请码',
        }

class TokenMeta(models.Model):
    first_date = models.DateTimeField()
    last_date = models.DateTimeField()
    price = models.FloatField(default=0.0002)
    supply = models.DecimalField(default=0, decimal_places=2, max_digits=10)
    band = models.IntegerField()    

class Ledger(models.Model):
    email = models.ForeignKey(InvestorProfile, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    token_debit = models.DecimalField(default=0, decimal_places=2, 
        max_digits=10)
    token_credit = models.DecimalField(default=0, decimal_places=2, 
        max_digits=10)
    token_bonus = models.DecimalField(default=0, decimal_places=2,
        max_digits=10)
    price = models.FloatField(default=0)
    txn_type = models.CharField(max_length=255)
    txn_hash = models.CharField(max_length=255, blank=True, null=True)
    wallet = models.CharField(max_length=50)
    completed = models.BooleanField(default=False)
    sold_to = models.CharField(blank=True, null=True, max_length=255)

class StoredTokens(models.Model):
    email = models.ForeignKey(InvestorProfile, on_delete=models.CASCADE)
    band = models.IntegerField() #numerical
    price = models.FloatField(default=0)
    amount = models.DecimalField(default=0, decimal_places=2, max_digits=12)
