from django.contrib import admin

from .models import InvestorProfile, TokenMeta, Ledger 

admin.site.register(InvestorProfile)
admin.site.register(TokenMeta)
admin.site.register(Ledger)
