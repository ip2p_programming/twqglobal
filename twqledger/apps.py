from django.apps import AppConfig


class TwqledgerConfig(AppConfig):
    name = 'twqledger'
