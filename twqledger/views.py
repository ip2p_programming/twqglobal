from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render
from django.urls import reverse
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from twqledger.models import (
    InvestorProfile, TokenMeta, Ledger, StoredTokens, RegistrationForm)
from .forms import LoginForm, BuyForm, TransferForm
import random, string, requests
from datetime import datetime, date
from decimal import Decimal

TEAM_LEADER_REFERRAL = 'YRQXWA7095'
MASTER_REFERRAL = 'SOD4817468'

def _access(request, ip):
    try:
        if not ip.logged_on:
            return False
    except Exception:
        return False
    return True
          
def index(request):
    message = ''
    if request.method == 'POST':
        ###REGISTRATION###
        if 'register' in request.POST:
            registerform = RegistrationForm(request.POST)
            loginform = LoginForm()
            cp = InvestorProfile.objects.filter(email=registerform['email'].value())
            pp = InvestorProfile.objects.filter(
                phone_number=registerform['phone_number'].value())
            if len(cp) > 0 or len(pp) > 0:
                 message = "An account has already been registered with that e-mail/phone number.i \n 此帐户已在此电子邮件地址/电话号码下注册。"
                 registerform = RegistrationForm()
                 return render(request, 'index.html', {
                     'registerform': registerform,
                     'loginform': loginform,
                     'message': message,
                 })
            if registerform.is_valid() and (
                registerform['password'].value()==registerform['password_confirm'].value()
            ):
                if registerform['referral'].value() != TEAM_LEADER_REFERRAL:
                    if registerform['referral'].value() != MASTER_REFERRAL:
                        try:
                            InvestorProfile.objects.get(
                                member_id=registerform['referral'].value(),
                                membership='TEAM LEADER')
                        except Exception:
                            registerform = RegistrationForm(request.POST)
                            message = "Referral ID is not valid. Please provide a valid team leader referral ID. \n 推荐標识号码无效。 請提供有效的團隊領導推荐標识号码"
                            return render(request, 'index.html', {
                                'registerform': registerform,
                                'loginform': loginform,
                                'message': message,
                            })
                bands = [1,2,3,4]
                registerform.save()
                ip = InvestorProfile.objects.get(email=registerform['email'].value())
                mem_str = ''.join(random.choice(string.ascii_uppercase) for x in range(3))
                mem_int = ''.join(random.choice(string.digits) for x in range(6))
                ip.member_id = mem_str + mem_int
                for band in bands:
                    meta = TokenMeta.objects.get(band=band)
                    StoredTokens.objects.create(email=ip, band=band, price=meta.price)
                
                if ip.referral == TEAM_LEADER_REFERRAL:
                    ip.membership = 'TEAM LEADER'
                elif ip.referral == MASTER_REFERRAL:
                    ip.membership = 'MASTER'
                else:
                    ip.membership = 'MEMBER'
                ip.save()
                message = "Please log in again to access the system \n 請再次登入系统"
                registerform = RegistrationForm()
                loginform = LoginForm()
                return render(request, 'index.html', {
                    'registerform': registerform,
                    'loginform': loginform,
                    'message': message,
                })
            else:
                message = "One of your fields is incorrect. Please provide a valid e-mail address or phone number while registering.\n 其中一个细节是不正確的 請在注册时提供一个有效的电子邮件地址/电话号码"                        
        ###LOGGING IN###
        elif 'login' in request.POST:
            loginform = LoginForm(request.POST)
            registerform = RegistrationForm()
            if loginform.is_valid():
                try:
                    ip = InvestorProfile.objects.get(email=loginform['email'].value())
                except Exception:
                    message = "Your email address does not belong in this system \n 你的电子邮件地址不属于此系统."
                    loginform = LoginForm()
                    return render(request, 'index.html', {
                            'registerform': registerform,
                            'loginform': loginform,
                            'message': message,
                        }
                    )
                if loginform['password'].value() == ip.password:
                    ip.logged_on = True
                    ip.save()
                    if ip.membership == 'MEMBER':
                        return HttpResponseRedirect(
                            reverse('portfolio', kwargs={'member': ip.member_id}))
                    elif ip.membership == 'TEAM LEADER' or ip.membership == 'TEST ACCOUNT':
                        return HttpResponseRedirect(
                            reverse('team', kwargs={'member': ip.member_id}))

                    elif ip.membership == 'MASTER':
                        return HttpResponseRedirect(
                            reverse('master', kwargs={'member': ip.member_id}))
                else:
                    message = "The password or e-mail address you provided is incorrect \n 你所提供的密码或电子邮件地址是不正確的" 
                    loginform = LoginForm()
                    return render(request, 'index.html', {
                            'registerform': registerform,
                            'loginform': loginform,
                            'message': message,
                        }
                    )
            else:
                message = "One of your fields is incorrect. Please provide a valid e-mail address or phone number while logging in.\n  其中一个细节是不正確的 請在登入系统时提供有效的电子邮件地址/电话号码  "                        
    else:
        registerform = RegistrationForm()
        loginform = LoginForm()

    return render(request, 'index.html', {
            'registerform': registerform,
            'loginform': loginform,
            'message': message,
        }
    )

def profile(request, ip):
    current_time = datetime.now()
    base_pot = 1
    #token_meta = TokenMeta.objects.get(band=base_pot)
    token_meta = TokenMeta.objects.get(
        first_date__lte=current_time, last_date__gte=current_time)
    
    # gets the next band if the supply for the current band is 0
    while token_meta.supply == 0:
        base_pot += token_meta.band;
        token_meta = TokenMeta.objects.get(band=base_pot)
    token_price = token_meta.price
    band = token_meta.band
    stored_tokens = StoredTokens.objects.filter(email=ip.email)
    # changed to filter to pick up the other bands. Let's change the bands numerically.

    data = {}
    data['date'] = datetime.now().date()
    data['email'] = ip.email
    data['member_id'] = ip.member_id
    data['buy_twq'] = settings.TWQ_CONTRACT_ADDRESS
    data['txn_fee_add'] = settings.TWQ_TRX_FEE_ADDRESS 
    data['price'] = token_price
    data['schedule'] = TokenMeta.objects.all()

    ledger_items = []
    stored_calc = {1:0,2:0,3:0,4:0}
    stored_bonus = 0
    ledger_data = Ledger.objects.filter(email=ip.email, completed=True)
    check_total_tokens = 0
    if len(ledger_data) > 0:
        for item in ledger_data:
            store = {}
            store['date'] = item.date
            store['debit'] = item.token_debit
            store['credit'] = item.token_credit
            store['price'] = item.price
            store['txn_type'] = item.txn_type
            store['bonus'] = item.token_bonus
            ledger_items.append(store)
            check_debit_price = 0
            check_credit_price = 0
            
            if item.token_debit > 0:
                check_debit_price = round((item.price/float(item.token_debit)), 6)
            elif item.token_credit > 0:
                check_credit_price = round((item.price/float(item.token_credit)), 6)

            if item.txn_type == 'Token Purchase' and item.token_bonus == 0 and item.price > 1:
                if item.price > 10:
                    item.token_bonus = item.token_debit * 20 / 100
                elif item.price > 5:
                    item.token_bonus = item.token_debit * 10 / 100
                elif item.price > 1:
                    item.token_bonus = item.token_debit * 5 / 100
                item.save()
                store['bonus'] = item.token_bonus
                
            stored_calc[band] += item.token_debit - item.token_credit
            stored_bonus += item.token_bonus
            
        if band > 1:
            for band_check in range (band -1):
                stored_calc[band] -= stored_tokens[band_check].amount
            
        data['ledger'] = ledger_items

    total_twq = 0
    for acc in stored_tokens:
        check_band = acc.band
        if stored_calc[check_band]:
            acc.amount = stored_calc[check_band]
            acc.save()
        total_twq += acc.amount
        
    data['twq_balance'] = round(total_twq, 2)
    
    data['twq_bonus'] = round(stored_bonus, 2)

    total_eth = 0
    for acc in stored_tokens:
        total_eth += (float(acc.amount) * acc.price)

    data['eth_investment'] = round(total_eth, 6)

    buyform = BuyForm()
    transferform = TransferForm()
    data['buyform'] = buyform
    data['transferform'] = transferform
    if request.method == 'POST' and len(request.POST) > 2:
        if 'next_buy' in request.POST:
            buyform, data['buymessage'], data['message'] = buy(
                request.POST, ip, token_price)
        if 'next_transfer' in request.POST:
            transferform, data['transfermessage'], data['message'] = transfer(
                request.POST, ip, token_price, stored_tokens)
    # verifying transactions
    escheck = [data['buy_twq'], data['txn_fee_add']]
    for es_address in escheck:            
        endpoint = settings.ETHERSCAN_ENDPOINT
        endpoint += 'module=logs'
        endpoint += '&action=getLogs'
        endpoint += '&fromBlock=%s' % settings.ETHERSCAN_START_BLOCK
        endpoint += '&toBlock=latest'
        endpoint += '&address=%s' % es_address
        endpoint += '&apikey=%s' % settings.ETHERSCAN_APIKEY
        response = requests.get(endpoint)
        etherscan_logs = response.json()
        if etherscan_logs['status'] == '1':
            for log in etherscan_logs['result']:
                eth_paid = int(log['data'][66:131],16) / 10 ** 18
                to_confirm = Ledger.objects.filter(
                    email=ip.email, price=eth_paid)
                if len(to_confirm) > 0:
                    for confirmation in to_confirm:
                        if ('x' in confirmation.wallet 
                            and confirmation.wallet.lower().split('x')[1] 
                            in log['data'] 
                            and not confirmation.txn_hash 
                        ):
                            if len(Ledger.objects.filter(
                                txn_hash=log['transactionHash'])) == 0:
                                confirmation.txn_hash = log['transactionHash']
                                confirmation.completed = 1
                                if confirmation.txn_type == "Token Purchase":
                                    token_meta.supply -= confirmation.token_debit
                                    token_meta.save()
                                confirmation.save()
                                if confirmation.sold_to:
                                    confirm_in = Ledger.objects.filter(
                                        email = confirmation.sold_to, 
                                        token_debit = confirmation.token_credit,
                                        completed = False,
                                    )
                                    confirm_txnhash = []
                                    for buyer in confirm_in:
                                        if confirmation.txn_hash not in confirm_txnhash:
                                            buyer.txn_hash = confirmation.txn_hash
                                            buyer.completed = True
                                            confirm_txnhash.append(buyer.txn_hash)
                                            buyer.save() 
    return data

def investor(request, member):
    ip = InvestorProfile.objects.get(member_id=member)
    access = _access(request, ip)
    if not access:
        return HttpResponseRedirect(reverse('index'))      
    
    data = profile(request, ip)

    return render(request, 'portfolio.html', data)

def teamlead(request, member):
    ip = InvestorProfile.objects.get(member_id = member)
    
    access = _access(request, ip)
    if not access:
        return HttpResponseRedirect(reverse('index'))      
    
    data = profile(request, ip)    

    new_users = []
    team_members = []
    ip = InvestorProfile.objects.filter(referral=member)
    for user in ip:
        team_members.append({'email': user.email, 'member_id':user. member_id})

    data['member_id'] = member
    data['members_list'] = team_members
    if len(team_members) > 0:
        for member in team_members:
            total_buy = 0
            total_transfer_in = 0
            total_transfer_out = 0
            total_bonus = 0
            member['total_twq'] = 0
            ledger = Ledger.objects.filter(email=member['email'], 
                completed=True)
            for item in ledger:
                if item.txn_type == 'Token Purc' or item.txn_type == 'Token Purchase':
                    total_buy += item.token_debit
                    total_bonus += item.token_bonus
                if item.txn_type == 'Transfer In':
                    total_transfer_in += item.token_debit
                if item.txn_type == 'Transfer Out' or item.txn_type == 'Token Sale':
                    total_transfer_out += item.token_credit
            member['total_twq'] += total_buy + total_transfer_in - total_transfer_out
            member['total_buy'] = total_buy
            member['total_transfer_in'] = total_transfer_in
            member['total_transfer_out'] = total_transfer_out
            member['total_bonus'] = total_bonus

    return render(request, 'teamportfolio.html', data)

def logout(request):
    if request.method == 'POST':
        ip = InvestorProfile.objects.get(member_id=request.POST['logout'])
        ip.logged_on = False
        ip.save()
    return HttpResponseRedirect(reverse('index'))

def transfer(post_data, ip, token_price, stored_tokens):
    transferform = TransferForm(post_data)
    message = None
    if transferform.is_valid():
        #next_page = Doesn't seem to be used anywhere and just declared.
        if '@' not in transferform['recipient'].value():
            try:
                recipient = InvestorProfile.objects.get(
                    member_id=transferform['recipient'].value())
            except Exception:
                transferform = TransferForm()
                transfermessage = {}
                message = "Invalid Member ID."
                return (transferform, transfermessage, message)
        
        elif '@' in transferform['recipient'].value():
            try:
                recipient = InvestorProfile.objects.get(
                    email=transferform['recipient'].value())
            except Exception:
                transferform = TransferForm()
                transfermessage = {}
                message = "Invalid email address."
                return (transferform, transfermessage, message)

        transfer_amount = Decimal(transferform['token_amount'].value())
        txn_fee = 0
        for pocket in range(4):
            stored_tokens[pocket].amount -= transfer_amount
            transfer_amount_remaining = 0
            if stored_tokens[pocket].amount < 0:
                transfer_amount_remaining = stored_tokens[pocket].amount * -1
                transfer_amount -= transfer_amount_remaining
                stored_tokens[pocket].amount = 0
            txn_fee += round(
                (float(transfer_amount) * stored_tokens[pocket].price),6)   
            transfer_amount = transfer_amount_remaining
            stored_tokens[pocket].save()

        Ledger.objects.create(
            email = ip,
            token_credit = transferform['token_amount'].value(),
            price = txn_fee / 5,
            txn_type = "Token Transfer Out",
            wallet = transferform['wallet'].value().lower(),
            sold_to = recipient.email,
        )

        Ledger.objects.create(
            email = recipient,
            token_debit = transferform['token_amount'].value(),
            txn_type = "Token Transfer In",
        )

        transfermessage = {
            'twqamount': transferform['token_amount'].value(),
            'buyer': "%s / %s" % (recipient.email, recipient.member_id),
            'txn_fee': txn_fee / 5, 
        }
        message = "Scroll down for payment details\n 往下參阅付款细节"
 
        return (transferform, transfermessage, message)

def buy(post_data, ip, token_price):
    buyform = BuyForm(post_data)
    if buyform.is_valid():
        bought_tokens = round((float(buyform['ether_amount'].value()) / token_price), 2)
        Ledger.objects.create(
            email = ip,
            price = buyform['ether_amount'].value(),
            token_debit = bought_tokens,
            txn_type = "Token Purchase",
            wallet = buyform['wallet'].value().lower(),
        )
            
        buymessage = {
            'twqamount': bought_tokens,
            'totalprice': buyform['ether_amount'].value(),
            'itwallet': buyform['wallet'].value(),
        }
        message = "Scroll down for payment details\n 往下參阅付款细节"

    return (buyform, buymessage, message)

def master(request, member):
    data = {}
    ip = InvestorProfile.objects.get(member_id=member)
    access = _access(request, ip)
    data['total_twq'] = 0
    data['total_eth'] = 0
    data['member_id'] = member
    if not access:
        return HttpResponseRedirect(reverse('index'))      
    teams = InvestorProfile.objects.filter(membership="TEAM LEADER")
    data['teamleaders'] = {}
    for teamleader in teams:
        token_data = StoredTokens.objects.filter(email=teamleader.email)
        tokens = 0
        cost = 0
        for token in token_data:
            tokens += token.amount
            cost += (float(token.amount) * token.price)
        data['total_twq'] += tokens
        data['total_eth'] += cost
        data['teamleaders'][teamleader.member_id] = {}
        data['teamleaders'][teamleader.member_id]['Own Account'] = [tokens, cost]
        members = InvestorProfile.objects.filter(referral=teamleader.member_id)
        if len(members) > 0:
            for member in members:
                token_data = StoredTokens.objects.filter(email=member.email)
                tokens = 0
                cost = 0
                for token in token_data:
                    tokens += token.amount
                    cost += round((float(token.amount) * token.price),6)
                data['total_twq'] += tokens
                data['total_eth'] += cost 
                data['teamleaders'][teamleader.member_id][member.member_id] = [tokens, cost]
    data['total_eth'] = round(data['total_eth'],6)     
    return render(request, 'master.html', data)
