# Generated by Django 2.0.4 on 2018-04-26 19:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twqledger', '0002_etherscan'),
    ]

    operations = [
        migrations.AddField(
            model_name='tokenmeta',
            name='band',
            field=models.CharField(default='A', max_length=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='storedtokens',
            name='bandA_balance',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='storedtokens',
            name='bandB_balance',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='storedtokens',
            name='bandC_balance',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='storedtokens',
            name='bandD_balance',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='storedtokens',
            name='bandE_balance',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='tokenmeta',
            name='price',
            field=models.FloatField(default=0.0002),
        ),
    ]
