# Generated by Django 2.0.4 on 2018-04-27 22:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twqledger', '0008_auto_20180427_2011'),
    ]

    operations = [
        migrations.AddField(
            model_name='ledger',
            name='txn_block',
            field=models.CharField(default=1, max_length=20),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ledger',
            name='price',
            field=models.FloatField(default=0.0002),
        ),
        migrations.AlterField(
            model_name='ledger',
            name='token_credit',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='ledger',
            name='token_debit',
            field=models.FloatField(default=0),
        ),
    ]
