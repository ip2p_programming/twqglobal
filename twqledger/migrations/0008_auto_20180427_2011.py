# Generated by Django 2.0.4 on 2018-04-27 20:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('twqledger', '0007_auto_20180427_1920'),
    ]

    operations = [
        migrations.AddField(
            model_name='tokenmeta',
            name='supply',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='etherscan',
            name='block_number',
            field=models.IntegerField(default=3115225),
        ),
    ]
