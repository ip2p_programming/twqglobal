from django.conf import settings

DEBUG = True

ALLOWED_HOSTS = settings.ALLOWED_HOSTS
ALLOWED_HOSTS.append('*')
DATABASES = settings.DATABASES
DATABASES['default']['NAME'] = 'twqglobal_dev'
DATABASES['default']['HOST'] = 'rm-zf8flfa599aj137d8.mysql.kualalumpur.rds.aliyuncs.com'
DATABASES['default']['USER'] = 'twq'

TWQ_CONTRACT_ADDRESS = '0x2dd82ef83fd1713ebfa66d611e68bb380410c8d2'
TWQ_TRX_FEE_ADDRESS = '0x341b1b684312fe684d841de91611fc6c89bdedf5'
ETHERSCAN_ENDPOINT = 'https://api-ropsten.etherscan.io/api?'
ETHERSCAN_START_BLOCK = 3152600
